import React from "react"

function LoadingTodoList() {
    return <h1>Your TodoList is currently loading... Please wait</h1>
}

export default LoadingTodoList