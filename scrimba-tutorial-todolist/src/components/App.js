import React from "react"

import Footer from "./Footer"
import MainContent from "./MainContent"
import Header from "./header"

import "../less/app.less"

class App extends React.Component {
    constructor() {
        super()
    }
    
    render() {
        return (
            <div>
                <Header />
                <MainContent />
                <Footer />
            </div>
        )   
    }
}

export default App