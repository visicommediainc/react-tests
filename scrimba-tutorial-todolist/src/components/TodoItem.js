import React from "react"
import "../less/todoItem.less"

class TodoItem extends React.Component {
    constructor(props) {
        super()
    }
    
    render() {
        return (
            <div className="todoItem">
                <input
                    id={this.props.item.id}
                    type="checkbox"
                    onChange={this.props.handleChange}
                    checked={this.props.item.completed}
                />
                <label htmlFor={this.props.item.id}>{this.props.item.text}</label>
            </div>
        )
    }
}

export default TodoItem