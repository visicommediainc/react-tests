import React from "react"
import TodoItem from "./TodoItem"
import LoadingTodoList from "./LoadingTodoList"
import todosData from "../data/todosData"

import "../less/todoList.less"

class MainContent extends React.Component {
    state = {
        todos: null,
        isLoading: true
    }

    constructor() {
        super()
    }

    componentDidMount() {
        fetch("https://swapi.co/api/people/1")
            .then(response => response.json())
            .then(data => console.log(data))
        
        setTimeout(() => {
            this.setState({
                todos: todosData,
                isLoading: false
            })
        }, 1000)
        
    }

    handleChange = (id) => {
        /*
        const {name, value, type, checked} = event.target
        type === "checkbox" ? this.setState({ [name]: checked }) : this.setState({ [name]: value })
        */
        this.setState(prevState => {
            const updatedTodos = prevState.todos.map(todo => {
                if(todo.id === id) {
                    todo.completed = !todo.completed
                }
                return todo
            })

            return {
                todos: updatedTodos
            }
        });
    }

    render() {
        const todoItems = this.state.todos ? this.state.todos.map(item => <TodoItem key={item.id} item={item} handleChange={() => this.handleChange(item.id)} />) : null
        return (
            <main>
                <div className="todoList">
                    {this.props.isLoading ? <LoadingTodoList /> : todoItems}
                </div>
            </main>
        )
    }
}

export default MainContent