import React from "react"
import Header from "./Header"
import MemeGenerator from "./MemeGenerator"

import styles from "../less/app.less"

class App extends React.Component {
    constructor() {
        super()
    }

    render() {
        return(
            <div>
                <Header />
                <MemeGenerator />
            </div>
        )
    }
}

export default App