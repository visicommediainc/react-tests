import React from "react"

class MemeGenerator extends React.Component {
    state = {
        topText: "",
        bottomText: "",
        image: "http://i.imgflip.com/1bij.jpg",
        allMemeImgs: []
    }
    
    constructor() {
        super()
    }

    componentDidMount() {
        fetch("https://api.imgflip.com/get_memes").
            then(response => response.json()).
            then(response => {
                const {memes} = response.data
                this.setState({
                    allMemeImgs: memes
                })
            })
    }

    handleChange = (event) => {
        const {id, value, type} = event.target
        type === "text" ? this.setState({[id]: value}) : this.setState({image: (this.state.allMemeImgs[Math.floor(Math.random() * Math.floor(this.state.allMemeImgs.length))]).url})
    }

    render() {
        return(
            <div>
                <form className="meme-form">
                    <input id="topText" type="text" placeholder="Top Text" value={this.state.topText} onChange={this.handleChange} />
                    <input id="bottomText" type="text" placeholder="Bottom Text" value={this.state.bottomText} onChange={this.handleChange} />
                    <button type="button" onClick={this.handleChange}>Gen</button>
                </form>

                <div className="meme">
                    <img src={this.state.image} alt="" />
                    <h2 className="top">{this.state.topText}</h2>
                    <h2 className="bottom">{this.state.bottomText}</h2>
                </div>
            </div>
        )
    }
}

export default MemeGenerator