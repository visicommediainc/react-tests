import React from "react"
import "../less/header.less"

class Header extends React.Component {
    constructor() {
        super()
    }
    
    render() {
        const firstName = "Sylz"
        const lastName = "Giroux"
        const date = new Date()

        return (
            <header className="navbar">Header {date.getHours() % 12} for {`${firstName} ${lastName}`}</header>
        )
    }
}

export default Header